<?php

namespace dynalogical\dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function __construct() {
        //$this->middleware('auth');
    }

    public function index() {
        $users = [];//User::all();

        return view('dashboard::users.index')->with(['users' => $users]);
    }

    public function create() {
        return view('dashboard::users.create');
    }

    public function show($id) {
        $user = User::find($id);
        return view('dashboard::users.show')->with(['user' => $user]);
    }

    public function edit($id) {
        $user = User::find($id);
        return view('dashboard::users.edit')->with(['user' => $user]);
    }

    public function store(Request $request) {
        $data = $request->all();
        $fullname = $data['first_name'] . ' ' . $data['last_name'];

        $user = new User();
        $user->first_name = $data["first_name"];
        $user->last_name = $data["last_name"];
        $user->email = $data["email"];
        $user->phone = $data["phone"];
        $user->user_token = DynalogicalController::generateRandomString(15);
        $user->rank = 1;
        $user->password = Hash::make($data["password"]);
        $user->save();

        Redirect::to(route('dashboard::users.index'))->with(["success" => 'Gebruiker "' . $fullname . '" toegevoegd']);
    }

    public function update(Request $request, $id) {
        $data = $request->all();
        $fullname = $data['first_name'] . ' ' . $data['last_name'];

        if ($data["password"] == $data["password2"]) {
            $user = User::find($id);
            $user->first_name = $data["first_name"];
            $user->last_name = $data["last_name"];
            if (Auth::user()->rank >= 2) { $user->rank = $data["rank"]; }
            $user->email = $data["email"];
            $user->phone = $data["phone"];
            if (!empty($data["password"])) { $user->password = Hash::make($data["password"]); }
            $user->save();

            Redirect::to(route('dashboard::users.edit', $id))->with(["success" => 'Gebruiker "' . $fullname . '" opgeslagen']);
        }

        Redirect::to(route('dashboard::users.edit', $id))->with(["error" => 'Er is iets mis gegaan. Wijzigingen niet opgeslagen']);
    }
}

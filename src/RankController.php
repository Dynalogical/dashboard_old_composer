<?php

namespace dynalogical\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use App\Models\Rank;

class RankController extends Controller
{
    public function __construct() {
        //$this->middleware('auth');
    }

    public function index() {
        $ranks = Rank::all();

        return view('dashboard::ranks.index')->with(['ranks' => $ranks]);
    }

    public function create() {
        return view('dashboard::ranks.create');
    }

    public function show($id) {
        $rank = Rank::find($id);

        return view('dashboard::ranks.show')->with(['rank' => $rank]);
    }

    public function edit($id) {
        $rank = Rank::find($id);
        return view('dashboard::ranks.edit')->with(['rank' => $rank]);
    }

    public function store(Request $request) {
        $data = $request->all();
        $rank_name = null;

        $rank = new Rank();
        $rank->rank_name = $data["rank_name"];
        $rank->is_customer = $data["is_customer"];
        $rank->is_admin = $data["is_admin"];
        $rank->is_superadmin = $data["is_superadmin"];
        $rank->save();

        $rank_name = $rank->rank_name;

        Redirect::to(route('dashboard::ranks.index'))->with(["success" => 'Rol "' . $rank_name . '" toegevoegd']);
    }

    public function update(Request $request, $id) {
        $data = $request->all();
        $rank_name = null;

        $rank = Rank::find($id);
        $rank->rank_name = $data["rank_name"];
        $rank->is_customer = $data["is_customer"];
        $rank->is_admin = $data["is_admin"];
        $rank->is_superadmin = $data["is_superadmin"];
        $rank->save();

        $rank_name = $rank->rank_name;

        Redirect::to(route('dashboard::ranks.edit', $id))->with(["success" => 'Rol "' . $rank_name . '" opgeslagen']);
    }
}

<?php
namespace dynalogical\dashboard;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RankSeeder extends Seeder
{
    public function run()
    {
        DB::table('rank')->insert([
            'rank_name' => 'Gebruiker',
            'is_customer' => '1',
            'is_admin' => '0',
            'is_superadmin' => '0',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('rank')->insert([
            'rank_name' => 'Administrator',
            'is_customer' => '1',
            'is_admin' => '1',
            'is_superadmin' => '0',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('rank')->insert([
            'rank_name' => 'Super Admin',
            'is_customer' => '1',
            'is_admin' => '1',
            'is_superadmin' => '1',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}

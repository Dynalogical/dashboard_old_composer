<?php
namespace dynalogical\dashboard;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Stef',
            'last_name' => 'van Zwienen',
            'email' => 'stef@dynalogical.nl',
            'phone' => '0640091508',
            'rank' => '3',
            'user_token' => '7QSDgQ97DuMZOZP',
            'password' => Hash::make('secret_stef'),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('users')->insert([
            'first_name' => 'Justin',
            'last_name' => 'Ripassa',
            'email' => 'justin@dynalogical.nl',
            'phone' => '0611174213',
            'rank' => '3',
            'user_token' => 't4XBApKlPf5xBDG',
            'password' => Hash::make('secret_justin'),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('users')->insert([
            'first_name' => 'Melvin',
            'last_name' => 'Brandjes',
            'email' => 'info@mebr.nl',
            'phone' => '0610996139',
            'rank' => '3',
            'user_token' => 'XNnFR8xoQIW0IJI',
            'password' => Hash::make('secret_melvin'),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('users')->insert([
            'first_name' => 'Jestin',
            'last_name' => 'Hoogeveen',
            'email' => 'jestin@cotive.net',
            'phone' => '0641061155',
            'rank' => '2',
            'user_token' => 'TwPO4TL0XYZ5K3K',
            'password' => Hash::make('secret_jestin'),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}

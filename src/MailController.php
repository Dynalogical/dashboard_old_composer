<?php

namespace dynalogical\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail as Mailer;
use Illuminate\Support\Facades\Redirect;

class MailController extends Controller
{
    public function Contact(Request $request) {
        $to_name = '';
        $to_email = '';
        $data = $request->all();
        $to_subject = config('mail.from.name')." | Contact aanvraag";

        Mailer::send('mail.contact', $data, function($message) use ($to_name, $to_email, $to_subject) {
            $message->to($to_email, $to_name)->subject($to_subject);
            $message->from(config('mail.from.name'), config('app.name'));
        });

        return Redirect::to(route('contact.index'))->with(["success" => "Uw contactformulier is verstuurd. " . config('app.name') . " neemt z.s.m. contact met u op."]);
    }
}

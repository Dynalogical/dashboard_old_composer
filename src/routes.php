<?php

//use Laravel\Fortify\Fortify;

Route::get('login', 'Dynalogical\Dashboard\DashboardController@index');
Route::get('register', 'Dynalogical\Dashboard\DashboardController@create');
Route::get('password-forgot', 'Dynalogical\Dashboard\DashboardController@forgot');

Route::get('dashboard', 'Dynalogical\Dashboard\dashboardController@index');

Route::get('nieuws', 'Dynalogical\Dashboard\NewsController@index');
Route::get('nieuws/create', 'Dynalogical\Dashboard\NewsController@create');
Route::get('nieuws/show/{id}', 'Dynalogical\Dashboard\NewsController@show');
Route::get('nieuws/edit/{id}', 'Dynalogical\Dashboard\NewsController@edit');

Route::get('users', 'Dynalogical\Dashboard\UserController@index');
Route::get('users/create', 'Dynalogical\Dashboard\UserController@create');
Route::get('users/show/{id}', 'Dynalogical\Dashboard\UserController@show');
Route::get('users/edit/{id}', 'Dynalogical\Dashboard\UserController@edit');

/*POST METHODS*/
Route::post('contact/sent/', 'Dynalogical\Dashboard\MailController@contact');

Route::post('nieuws/store', 'Dynalogical\Dashboard\NewsController@store');
Route::post('nieuws/update/{id}', 'Dynalogical\Dashboard\NewsController@update');

Route::post('users/store', 'Dynalogical\Dashboard\UserController@store');
Route::post('users/update/{id}', 'Dynalogical\Dashboard\UserController@update');

/*Fortify::loginView(function () { return view('auth.index'); });
Fortify::registerView(function () { return view('auth.register'); });
Route::get('logout', function () { auth()->logout(); Session()->flush(); return Redirect::to('/'); })->name('logout');*/

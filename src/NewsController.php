<?php

namespace dynalogical\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\News;

class NewsController extends Controller
{
    public function __construct() {
        //$this->middleware('auth' ['except' => ['index']);
    }

    public function index() {
        $news = News::all();

        return view('dashboard::news.index')->with(['news' => $news]);
    }

    public function create() {
        return view('dashboard::news.create');
    }

    public function show($id) {
        $news = News::find($id);

        return view('dashboard::news.show')->with(['news' => $news]);
    }

    public function edit($id) {
        $news = News::find($id);

        return view('dashboard::news.edit')->with(['news' => $news]);
    }

    public function store(Request $request) {
        $data = $request->all();
        $image_path = null;
        $image_path = DynalogicalController::ImageUpload($data["image_path"]);

        var_dump($image_path);

        die();

        $news = new News();
        $news->user_id = $data["user_id"];
        $news->image_path = $image_path;
        $news->title = $data["title"];
        $news->sub_title = $data["sub_title"];
        $news->news_item = $data["news_item"];
        $news->save();

        Redirect::to(route('dashboard::news.index'))->with(["success" => "Nieuwsartikel opgeslagen"]);
    }

    public function update(Request $request, $id) {
        $data = $request->all();

        $news = News::find($id);
        $news->user_id = $data["user_id"];
        $news->image_path = $data["image_path"];
        $news->title = $data["title"];
        $news->sub_title = $data["sub_title"];
        $news->news_item = $data["news_item"];
        $news->save();

        Redirect::to(route('dashboard::news.edit', $id))->with(["success" => "Nieuwsartikel opgeslagen"]);
    }
}

<?php

namespace dynalogical\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use \Illuminate\Support\Facades\Request as FileRequest;
use Illuminate\Http\Request;

class DynalogicalController extends Controller
{
    public static function generateRandomString($length = 15) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function ImageUpload(FileRequest $image_path) {
        if(FileRequest::hasFile($image_path)) {
            $file = $image_path;
            $unique = DynalogicalController::generateRandomString(10);

            if ($file->isValid()) {
                $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $file_ext = $file->extension();
                $image_path = 'uploads/'.$unique.'/'.$file_name.'.'.$file_ext;
                $file->move('/uploads/'.$unique, $file_name.'.'.$file_ext);

                return ['success' => $image_path];
            }

            return ['error' => 'File is invalid'];
        }

        return ['error' => 'File is not a file'];
    }

    public static function documentUpload($file_path) {

    }
}

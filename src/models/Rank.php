<?php

namespace App\Http\Controllers\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rank extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "rank";

    protected $fillable = [
        'rank_name',
        'is_customer',
        'is_admin',
        'is_superadmin',
    ];
}

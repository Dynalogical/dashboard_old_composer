<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>{{ config('app.name') }} - @yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">
        <!-- Custom Stylesheet -->
        <link type="text/css" rel="stylesheet" href="{{ asset('css/dynalogical.css') }}">
    </head>
    <body>
        <main>
            @yield('content')
        </main>
    </body>
    <!-- Footer start -->
    <footer class="footer">
        <script  src="{{ asset('js/dynalogical.js') }}"></script>
    </footer>
</html>

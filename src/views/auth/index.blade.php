<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name') }} | Inloggen</title>
    <link href="https://unpkg.com/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
    <style>
        .body-bg {
            background-color: #E17632; /*#9921e8*/
            background-image: linear-gradient(160deg, #E17632 35%, #EDA64C 100%);/*linear-gradient(315deg, #9921e8 0%, #5f72be 74%);*/

        }
    </style>
</head>
<body class="body-bg min-h-screen pt-12 md:pt-20 pb-6 px-2 md:px-0" style="font-family:'Lato',sans-serif;">
<header class="max-w-lg mx-auto">
    <a href="#">
        <h1 class="text-4xl font-bold text-white text-center">{{ config('app.name') }}</h1>
    </a>
</header>

<main class="bg-white max-w-lg mx-auto p-8 md:p-12 my-10 rounded-lg shadow-2xl">
    <section>
        <h3 class="font-bold text-2xl">Welkom!</h3>
        <p class="text-gray-600 pt-2">Gebruik onderstaande velden om in te loggen.</p>
    </section>

    @if(isset($errors)  && count($errors) > 0)
        <section>
            <p>{{ $errors }}</p>
        </section>
    @endif

    <section class="mt-10">
        <form class="flex flex-col" method="POST" action="{{--{{ route('login') }}--}}">
            @csrf
            <div class="mb-6 pt-3 rounded bg-gray-200">
                <label class="block text-gray-700 text-sm font-bold mb-2 ml-3" for="email">Email</label>
                <input type="email" id="email" name="email" class="bg-gray-200 rounded w-full text-gray-700 focus:outline-none border-b-4 border-gray-300 focus:border-yellow-500 transition duration-500 px-3 pb-3">
            </div>
            <div class="mb-6 pt-3 rounded bg-gray-200">
                <label class="block text-gray-700 text-sm font-bold mb-2 ml-3" for="password">Wachtwoord</label>
                <input type="password" id="password" name="password" class="bg-gray-200 rounded w-full text-gray-700 focus:outline-none border-b-4 border-gray-300 focus:border-yellow-500 transition duration-500 px-3 pb-3">
            </div>
            <div class="flex justify-end">
                <a href="#" class="text-sm text-yellow-600 hover:text-yellow-700 hover:underline mb-6">Wachtwoord vergeten?</a>
            </div>
            <button class="bg-yellow-500 hover:bg-yellow-600 text-white font-bold py-2 rounded shadow-lg hover:shadow-xl transition duration-200" type="submit">Inloggen</button>
        </form>
    </section>
</main>
<footer class="h-8 w-56 mx-auto flex justify-center text-white">
    <img src="{{ asset('img/dynalogical_logo_white.png') }}">
</footer>
</body>
</html>

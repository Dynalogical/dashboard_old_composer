<?php

namespace Dynalogical\Dashboard;

//use Illuminate\Support\Facades\Config;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class DynalogicalServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /*Controllers*/
        $this->app->make('Dynalogical\Dashboard\DashboardController');
        $this->app->make('Dynalogical\Dashboard\NewsController');
        $this->app->make('Dynalogical\Dashboard\ContactController');
        $this->app->make('Dynalogical\Dashboard\UserController');

        /*Models*/
        $this->app->make('Dynalogical\Dashboard\models\News');
        $this->app->make('Dynalogical\Dashboard\models\User');
        $this->app->make('Dynalogical\Dashboard\models\Rank');

        /*Views*/
        $this->loadViewsFrom(__DIR__.'/views/auth', 'login');
        $this->loadViewsFrom(__DIR__.'/views/dashboard', 'dashboard');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';

        $this->publishes([
            __DIR__.'/DashboardController.php' => app_path('Http/Controllers/DashboardController.php'),
            __DIR__.'/NewsController.php' => app_path('Http/Controllers/NewsController.php'),
            __DIR__.'/ContactController.php' => app_path('Http/Controllers/ContactController.php'),
            __DIR__.'/UserController.php' => app_path('Http/Controllers/UserController.php'),

            __DIR__.'/models/News.php' => app_path('Models/News.php'),
            __DIR__.'/models/User.php' => app_path('Models/User.php'),
            __DIR__.'/models/Rank.php' => app_path('Models/Rank.php'),

            __DIR__.'/views' => $this->app->basePath() . '/resources/views', 'resources',
            __DIR__.'/public' => $this->app->basePath() . '/public', 'public',
            __DIR__.'/migrations' => $this->app->databasePath() . '/migrations', 'migrations',
            __DIR__.'/seeders' => $this->app->databasePath() . '/seeders', 'seeders',
        ]);

        $this->loadMigrationsFrom(__DIR__.'/migrations');
    }
}
